package sourceit.com.catalogofcountries;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ListAdapter extends BaseAdapter {

    List<Region> list;
    Context context;

    public ListAdapter(Context context, List<Region> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Region getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View root = convertView;

        if (root == null) {
            root = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
            viewHolder = new ViewHolder(root);
            root.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) root.getTag();
        }

        Region region = getItem(position);
        viewHolder.region.setText(region.region);

        return root;
    }

    static class ViewHolder {
        TextView region;

        ViewHolder(View view) {
            region = (TextView) view.findViewById(R.id.name);
        }
    }
}
