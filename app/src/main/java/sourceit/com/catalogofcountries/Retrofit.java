package sourceit.com.catalogofcountries;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        //        @GET("/v1/all")
//        void getCountries(Callback<List<Country>> callback);
        @GET("/v2/all")
        void getRegions(Callback<List<Region>> callback);
//        @GET("/v2/name/{name}")
//        void getCountriesByName(@Path(value = "name") String name, Callback<List<Country>> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }


    public static void getRegions(Callback<List<Region>> callback) {
        apiInterface.getRegions(callback);
    }


}
